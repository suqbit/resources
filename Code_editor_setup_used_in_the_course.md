إعداد محرر الأكواد المستخدم في الدورة

Brakets (الجزء الأول من الدورة) 

Theme
أستخدم OS X Style | Flat & Dark Brackets theme خلال الجزء الأول من الدورة.

الإضافات المستخدمة:

  للحفظ التلقائي للملفات عند استخدام محرر الأكواد. Autosave Files on Window Blur  


ليعرض أيقونات أنواع الملفات ملونة في شجرة الملفات. Brackets Icons    

VSCode (الجزء الثاني من الدورة)

Theme
 
[Link →](https://marketplace.visualstudio.com/items?itemName=naumovs.theme-oceanicnext) Dimmed bg مع خيار  Oceanic Nextأستخدم

الإضافات المستخدمة:

إقرأ العرض العام لكل إضافة لمعرفة كيفية استخدامهم

Auto Close Tag لإغلاق ال html tags  تلقائيا[. Link ](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag)→

Auto Rename Tag لتغيير ال HTML tagsالمتماثلة تلقائيا . [Link →](https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag)

Color Highlight لتحديد الألوان في CSS.[ Link ](https://marketplace.visualstudio.com/items?itemName=naumovs.color-highlight)→

Paste and Indent لترك مسافة تلقائيا عند لصق كود. [Link →](https://marketplace.visualstudio.com/items?itemName=Rubymaniac.vscode-paste-and-indent)

Path Intellisense ملئ تلقائي لأسماء الملفات..[ Link →](https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense)

Prettier لتنسيق الكود تلقائيا. [Link →](https://marketplace.visualstudio.com/items?itemName=esbenp.prettier-vscode)


الإعدادات:

إذا أردت أن يبدو محرر الأكواد الخاص بك ويعمل بالضبط مثل محرر الأكواد لدي في الدورة، يمكنك نسخ هذه الإعدادات لملف الإعدادات لديك. فقط اذهب إلى VSCode، ثم في الجهة اليمنى قم بلصق هذا الكود.

```
{
  "workbench.colorTheme": "Oceanic Next (dimmed bg)",
  "files.autoSave": "onFocusChange",
  "editor.minimap.enabled": true,
  "workbench.statusBar.visible": true,
  "workbench.activityBar.visible": true,
  "editor.formatOnSave": false,

  "workbench.colorCustomizations": {
    "statusBar.background": "#333333",
    "statusBar.noFolderBackground": "#333333",
    "statusBar.debuggingBackground": "#263238"
  },
  "editor.fontSize": 16,

  "css.validate": false,
  "scss.validate": false,
  "less.validate": false,
  "editor.wordWrap": "on"
}
```

